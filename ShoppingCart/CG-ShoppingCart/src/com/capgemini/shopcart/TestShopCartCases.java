package com.capgemini.shopcart;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestShopCartCases extends TestCase {
	
	/**
	 *
	 * @author pNobre
	 */

	  private static String singleBuyApples 				= "Apples";
	  private static String singleBuyOranges				= "Oranges";
	  private static String multipleBuy 					= "Apples Oranges Oranges Apples Apples";
	  private static String notExistentItem					= "Bananas";
	  private static String noBuy							= "";
	  private static String multipleBuymultipleSeparators 	= "Apples;Oranges:Oranges,Apples Oranges";
	  private static String largeQuantitymultipleBuymultipleSeparators 	= 
			  													"Apples;Oranges:Oranges,Apples Oranges "+
			  													"Apples;Oranges:Oranges,Apples Oranges "+
			  													"Apples;Oranges:Oranges,Apples Apples "+
			  													"Oranges:Oranges";
	  

	  public TestShopCartCases(java.lang.String testName) {
	    super(testName);
	  }

	  public static void main(java.lang.String[] args) {
	    junit.textui.TestRunner.run(suite());
	  }

	  public static Test suite() {
	    TestSuite suite = new TestSuite(TestShopCartCases.class);
	    return suite;
	  }

	  public void testSingleBuyApples() {
	    try {
	      ShoppingCart shop = new ShoppingCart(singleBuyApples);
	      assertEquals(0.60, shop.getTotalCheckOut());
	    }
	    catch (Exception e) {
	      fail(e.getMessage());
	    }
	  }

	  public void testSingleBuyOranges() {
	    try {
	      ShoppingCart shop = new ShoppingCart(singleBuyOranges);
	      assertEquals(0.25, shop.getTotalCheckOut());
	    }
	    catch (Exception e) {
	      fail(e.getMessage());
	    }
	  }

	  public void testMultipleBuy() {
			try {
				ShoppingCart shop = new ShoppingCart(multipleBuy);
				assertEquals(1.70, shop.getTotalCheckOut());
			}
			catch (Exception e) {
				fail(e.getMessage());
			}
	  }

	  public void testNonExistent() {
			try {
				ShoppingCart shop = new ShoppingCart(notExistentItem);
				assertEquals(0.0, shop.getTotalCheckOut());
			}
			catch (Exception e) {
				fail(e.getMessage());
			}
	  }
	  
	  public void testNoBuy() {
			try {
				ShoppingCart shop = new ShoppingCart(noBuy);
				assertEquals(0.0, shop.getTotalCheckOut());
			}
			catch (Exception e) {
				fail(e.getMessage());
			}
	  }
	  public void testMultipleBuyMultipleSeparators() {
			try {
				ShoppingCart shop = new ShoppingCart(multipleBuymultipleSeparators);
				assertEquals(1.1, shop.getTotalCheckOut());
			}
			catch (Exception e) {
				fail(e.getMessage());
			}
	  }
	  public void testLargeQuantityMultipleBuyMultipleSeparators() {
			try {
				ShoppingCart shop = new ShoppingCart(largeQuantitymultipleBuymultipleSeparators);
				assertEquals(4.15, shop.getTotalCheckOut());
			}
			catch (Exception e) {
				fail(e.getMessage());
			}
	  }
}
