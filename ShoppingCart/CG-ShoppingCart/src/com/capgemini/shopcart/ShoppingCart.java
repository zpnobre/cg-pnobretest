/*
 *
 */
package com.capgemini.shopcart;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ShoppingCart {
	
	private HashMap<String, Double> priceList = new HashMap<String, Double>()
	{
		private static final long serialVersionUID = 8759176688813385021L;
		{put("apples",0.60);put("oranges",0.25);}
	};
	
	private HashMap<String, Integer> promoList = new HashMap<String, Integer>()
	{
		private static final long serialVersionUID = 1L;
		{put("apples",2);put("oranges",3);}
	};
	
	private Double totalCheckOut=0.0;
	
	public ShoppingCart(String listOfItems) {
		totalCheckOut = this.checkOut(listOfItems);
	}
	
	private Long qtyDiscount(String item, Long Quantity) {
		Integer promoFactor = promoList.get(item.toLowerCase());
		
		if (promoFactor==null)
			return new Long(0);
		return (Long)(Quantity/promoFactor);
	}
	
	public Double checkOut(String s) {
		Map<String, Long> counted = Arrays
				.stream(s.split(" |,|;|:"))
				.parallel()
				.collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
		Double price=null,totalSum=0.0;
		
		for ( Entry<String, Long> entry : counted.entrySet()) {
			price = priceList.get(entry.getKey().toLowerCase());
			if (price != null)
				totalSum += (entry.getValue() - qtyDiscount(entry.getKey(),entry.getValue())) * price;
		}
		return totalSum;	
    }
	
	public Double getTotalCheckOut() {
		return totalCheckOut;
	}
	
}


